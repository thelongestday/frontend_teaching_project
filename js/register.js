function init(){
    //email检验置空
    document.getElementById("email").addEventListener("focus",()=>{
        document.querySelector(".email_error_msg").innerHTML="";
        document.querySelector(".email_error_img").classList.remove("errorImg");
        document.querySelector(".email_error_msg").classList.remove("errorMessage");
        document.querySelector(".email_error_msg").classList.remove("okMessage");
        document.querySelector(".email_error_img").classList.remove("okImg");
    },false);
    //email检验
    document.getElementById("email").addEventListener("blur",()=>{
        let regExp=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
        if(!regExp.test(document.getElementById("email").value)){
            document.querySelector(".email_error_msg").innerHTML="邮箱格式不正确！";
            document.querySelector(".email_error_msg").classList.add("errorMessage");
            document.querySelector(".email_error_img").classList.add("errorImg");
        }else{
            document.querySelector(".email_error_msg").innerHTML="邮箱格式符合要求！";
            document.querySelector(".email_error_msg").classList.add("okMessage");
            document.querySelector(".email_error_img").classList.add("okImg");
            
        }
    },false);
}
window.addEventListener("load",init,false);